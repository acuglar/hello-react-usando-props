import React from 'react';
import TitleNote from './TitleNote';

class SubTitleNote extends TitleNote {
  render() {
    return (
      <div>
        {this.props.text}
        {this.props.feeling}
      </div>
    )
  }
}

export default SubTitleNote;